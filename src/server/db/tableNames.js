const tableNames = [
	'character',
	'characterList',
	'stash',
	'skins',
	'login',
	'leaderboard',
	'customMap',
	'customChannels',
	'error',
	'modLog',
	'accountInfo',
	'recipes'
];

module.exports = tableNames;
